# The image for prediction model web-service
FROM python:3.9

WORKDIR /code

# Python dependencies are managed by poetry
RUN pip install --upgrade pip
RUN pip install poetry
COPY pyproject.toml poetry.lock /code/
RUN poetry config virtualenvs.create false && \
    poetry install --no-dev --no-interaction --no-ansi

# copy src and config files
COPY src/ /code/src
COPY conf/ /code/conf

# start web-server
CMD ["uvicorn", "src.app.inference:app", "--host", "0.0.0.0", "--port", "80"]
#CMD ls -d