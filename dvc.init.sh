source .env

poetry run dvc remote add --local -d -f dvc-minio s3://$DVC_S3_BUCKET
poetry run dvc remote modify --local dvc-minio endpointurl http://localhost:9000
poetry run dvc remote modify --local dvc-minio access_key_id $AWS_ACCESS_KEY_ID
poetry run dvc remote modify --local dvc-minio secret_access_key $AWS_SECRET_ACCESS_KEY