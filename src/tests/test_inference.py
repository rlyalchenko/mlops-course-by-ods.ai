import asyncio
import re

import pandas as pd
from pydantic import parse_raw_as

from src.app.inference import get_model_version, predict_proba
from src.app.session_info_request import SessionInfoRequest
from src.configuration.config import get_config


def test_predict():
    """Checks that model service predict method returns correct number of raws,
    returned data contains the same sessions as in request,
    probabilities sum is 1 for each session
    """
    cfg = get_config()
    df = pd.read_csv("data/unittest/train_sessions.csv", parse_dates=cfg.data.date_cols).head(100)
    sessions_json = df.to_json(orient="records", date_format="iso")
    sessions_list = parse_raw_as(list[SessionInfoRequest], sessions_json)
    predict_coro = predict_proba(sessions_list)
    predictions = asyncio.run(predict_coro)
    # check length of response is equal to request
    assert (
        len(predictions) == df.shape[0]
    ), "Number of sessions in request and response does not match"
    # check session_ids are the same in the same order
    df_predictions = pd.DataFrame(list(dict(prediction) for prediction in predictions))
    assert all(
        df["session_id"] == df_predictions["session_id"]
    ), "session_id order in request/response is not the same"
    # check sum of all probabilities for each session is 1
    df_predictions.set_index("session_id", inplace=True)
    assert all(
        df_predictions.sum(axis=1) == 1
    ), "There are session where sum of predicted classses is not 1"


def test_get_model_info():
    """Checks that get_model_info contains required tags in response header"""
    model_info_coro = get_model_version()
    model_info = asyncio.run(model_info_coro)
    assert model_info, "model_info returned empty result"
    assert model_info.headers["etag"], "response does not contain etag in response header"
    assert re.match(
        "Transformer: mlflow-artifacts:.+, Predictor: mlflow-artifacts:.+",
        model_info.headers["etag"],
    ), "get_model_infor response does not match patter 'Transformer: {path}, Predictor: {path}"


if __name__ == "__main__":
    test_get_model_info()
    test_predict()
