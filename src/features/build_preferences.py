import pickle

import click
import joblib as jb
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from src.configuration import get_config
from src.features import UserPreferenceFeaturesBuilder


@click.command()
@click.argument("in_site_dict", type=click.Path(exists=True))
@click.argument("in_train", type=click.Path(exists=True))
@click.argument("out_train_preference", type=click.Path())
@click.argument("out_transformer", type=click.Path())
def build_preferences(
    in_site_dict: str,
    in_train: str,
    out_train_preference: str,
    out_transformer: str,
):
    """The method builds datasets with user preferences flag for target user and
    the rest of the users

    Args:
        in_site_dict (str): path to file with {site: code} dictionary (pkl)
        in_train (str): path to train dataset (csv)
        out_train_preference (str): path to train output file with user preferences (npz)
        out_transformer (str): path to exported transformer (pkl)
    """
    # load sites dictionary {site: code} and prepare reversed dictionary {code: site}
    with open(in_site_dict, "rb") as site_dict_file:
        site_dict = pickle.load(site_dict_file)
    site_dict_reverse = {code: site for (site, code) in site_dict.items()}

    cfg = get_config()
    interests_pipe = Pipeline(
        [
            (
                "interest_prep",
                UserPreferenceFeaturesBuilder(cfg.data.site_cols, site_dict_reverse, cfg.eda),
            ),
            ("interest_scale", StandardScaler()),
        ]
    )

    # train dataset
    df_train = pd.read_csv(
        in_train, index_col=cfg.data.index_col, parse_dates=cfg.data.date_cols
    )
    y = df_train[cfg.data.target_col]
    train_preferences = interests_pipe.fit_transform(df_train, y)
    X_train_preference = pd.DataFrame(data=train_preferences, columns=["pref_target", "pref_other"])
    X_train_preference.to_csv(out_train_preference)

    # dump pipeline
    jb.dump(interests_pipe, out_transformer)


if __name__ == "__main__":
    build_preferences()  # pylint: disable=no-value-for-parameter
