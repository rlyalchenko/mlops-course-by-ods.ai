import click
import joblib as jb
import mlflow
import pandas as pd
from scipy.sparse import hstack, load_npz, save_npz
from sklearn.pipeline import FeatureUnion

from src.configuration.config import get_config
from src.models.mlflow_helper import STAGE_STAGING, save_sklearn_model


@click.command()
@click.argument("in_train_site", type=click.Path(exists=True))
@click.argument("in_train_preference", type=click.Path(exists=True))
@click.argument("in_train_feature", type=click.Path(exists=True))
@click.argument("in_transformer_site", type=click.Path(exists=True))
@click.argument("in_transformer_preference", type=click.Path(exists=True))
@click.argument("in_transformer_feature", type=click.Path(exists=True))
@click.argument("out_train_full", type=click.Path())
@click.argument("out_transformer_full", type=click.Path())
@click.option("--skip-mlflow", is_flag=True, default=False)
def build_union(
    in_train_site: str,
    in_train_preference: str,
    in_train_feature: str,
    in_transformer_site: str,
    in_transformer_preference: str,
    in_transformer_feature: str,
    out_train_full: str,
    out_transformer_full: str,
    skip_mlflow: bool,
):
    """Combines all preprocessed datasets in full-featured train and test datasets

    Args:
        in_train_site (str): path to file with train sites dataset (npz)
        in_train_preference (str): path to file with train preferences dataset (csv)
        in_train_feature (str): path to file with train additional features dataset (npz)
        in_transformer_site (str): path to file with sites transformer (pkl)
        in_transformer_preference (str): path to file with preferences transformaer (pkl)
        in_transformer_feature (str): path to file with features transformer (pkl)
        out_train_full (str): path to output file with train sparse matrix with all features (npz)
        out_transformer_full (str): output path to transformer that creates all features (pkl)
        skip_mlflow (bool): do not save model in mlflow, use for unittests only
    """
    cfg = get_config()

    # concatenate train dataset
    X_train_sites = load_npz(in_train_site)
    X_train_preferences = pd.read_csv(in_train_preference, index_col=0)
    X_train_features = load_npz(in_train_feature)
    X_train = hstack([X_train_sites, X_train_preferences, X_train_features])
    save_npz(out_train_full, X_train)

    # concatenate full feature transformer pipeline
    sites_pipe = jb.load(in_transformer_site)
    preferences_pipe = jb.load(in_transformer_preference)
    feature_pipe = jb.load(in_transformer_feature)
    full_pipe = FeatureUnion(
        transformer_list=[
            ("sites", sites_pipe),
            ("pref", preferences_pipe),
            ("feat", feature_pipe),
        ]
    )

    # dump model in local file
    jb.dump(full_pipe, out_transformer_full)

    # dump model in mlflow as new Staging transformer
    if not skip_mlflow:
        mlflow.set_tracking_uri(cfg.mlflow.tracking_uri)
        save_sklearn_model(
            model=full_pipe,
            name=cfg.mlflow.transformer_model_name,
            experiment_name=cfg.mlflow.transformer_experiment_name,
            stage=STAGE_STAGING,
        )


if __name__ == "__main__":
    build_union()  # pylint: disable=no-value-for-parameter
