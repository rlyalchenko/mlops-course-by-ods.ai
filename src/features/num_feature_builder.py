import datetime

import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin

from src.configuration.config import DataConfig, EDAConfig


class NumFeatureBuilder(BaseEstimator, TransformerMixin):
    """
    Creates additional numeric features
    ------------------
    - session_time - session duration in seconds
    - time_per_site - average time per site
    - is_alice_time - binary feature. 1 if Alice is active in the perion of time
    - is_evening - binary feature. 1 if it's evening time
    - is_morning - binary feature. 1 is it's morning time
    """

    def __init__(self, data_cfg: DataConfig, eda_cfg: EDAConfig):
        self.site_columns = data_cfg.site_cols
        self.date_columns = data_cfg.date_cols
        self.eda_cfg = eda_cfg
        super().__init__()

    # pylint: disable=unused-argument
    def fit(self, X: pd.DataFrame, y=None) -> "NumFeatureBuilder":
        return self

    def transform(self, X: pd.DataFrame, y=None) -> np.ndarray:
        sites_count = len(self.site_columns) - np.sum(X[self.site_columns].isna(), axis=1)
        # do not use np.min/np.max
        # they both return NaN, if datetime contains timezone information
        # and there is at least one NaT value
        # numeric_only=False is workaround to make pd.Dataframe.sum() work with NaT values
        session_time = (
            (
                X[self.date_columns].max(axis=1, numeric_only=False)
                - X[self.date_columns].min(axis=1, numeric_only=False)
            ).dt.seconds
        ) ** 0.1
        time_per_site = (session_time / sites_count) ** 0.1
        is_alice_time = X[self.date_columns[0]].apply(self.is_alice_time)
        is_evening = (X[self.date_columns[0]].dt.time > datetime.time(18, 30)).astype(int)
        is_morning = (X[self.date_columns[0]].dt.hour <= 11).astype(int)
        return np.c_[session_time, time_per_site, is_alice_time, is_evening, is_morning]

    def is_alice_time(self, dt: pd.Timestamp) -> int:
        """
        The method returns 1 if time belongs to Alice's standard working hours,
        otherwise - 0
        """
        # for start, end in timetable[dt.weekday()]:
        for start, end in self.eda_cfg.alice_timetable[dt.day_name()]:
            if dt.time() >= start and dt.time() <= end:
                return 1
        return 0
