import click
import joblib as jb
import pandas as pd
from scipy.sparse import save_npz
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline

from src.configuration import get_config
from src.features import SitesBuilder


@click.command()
@click.argument("in_train", type=click.Path(exists=True))
@click.argument("out_train_site", type=click.Path())
@click.argument("out_transformer", type=click.Path())
def build_sites(
    in_train: str,
    out_train_site: str,
    out_transformer: str,
):
    """Builds sparse matricies of TF-IDF sites features

    Args:
        in_train (str): path to input train dataset (csv)
        out_train_site (str): path to output train file with extracted sites features (npz)
        out_transformer (str): path to exported sites transformer (pkl)
    """

    cfg = get_config()
    sites_pipe = Pipeline(
        [
            ("site_prep", SitesBuilder(cfg.data)),
            (
                "site_vect",
                TfidfVectorizer(
                    # word starts with any digit except 0
                    # then any number of alpha-numeric symbols
                    # delimiter is whitespace
                    # default pattern does not allow 1 symbol words
                    token_pattern=r"(?u)[1-9]\w*\b",
                    ngram_range=(1, 3),
                    max_features=50000,
                    sublinear_tf=True,
                ),
            ),
        ]
    )

    df_train = pd.read_csv(
        in_train, index_col=cfg.data.index_col, parse_dates=cfg.data.date_cols
    )
    sites_pipe.fit(df_train)
    X_train_sites = sites_pipe.transform(df_train)
    save_npz(out_train_site, X_train_sites)

    jb.dump(sites_pipe, out_transformer)


if __name__ == "__main__":
    build_sites()  # pylint: disable=no-value-for-parameter
