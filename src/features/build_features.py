import click
import joblib as jb
import pandas as pd
from scipy.sparse import save_npz
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.preprocessing import OneHotEncoder, StandardScaler

from src.configuration import get_config
from src.features import CatFeaturesBuilder, NumFeatureBuilder


@click.command()
@click.argument("in_train", type=click.Path(exists=True))
@click.argument("out_train_feature", type=click.Path())
@click.argument("out_transformer", type=click.Path())
def build_features(
    in_train: str,
    out_train_feature: str,
    out_transformer: str
):
    """Method creates additional categorical and numeric features

    Args:
        in_train (str): path to input train dataset (csv)
        out_train_feature (str): path to output train file with extracted features (npz)
        out_transformer (str): path to exported feature transformer (pkl)
    """
    cfg = get_config()
    # categorical features pipeline
    cat_features_pipe = Pipeline(
        [
            ("cat_feat_prep", CatFeaturesBuilder(cfg.data)),
            ("cat_feat_enc", OneHotEncoder(handle_unknown="ignore")),
        ]
    )
    # numeric features pipeline
    num_features_pipe = Pipeline(
        [
            ("num_feat_prep", NumFeatureBuilder(cfg.data, cfg.eda)),
            ("num_feat_scale", StandardScaler()),
        ]
    )
    # categorical and numeric features union
    features_pipe = FeatureUnion(
        transformer_list=[
            ("cat_feat", cat_features_pipe),
            ("num_feat", num_features_pipe),
        ]
    )
    # add features for train data
    df_train = pd.read_csv(
        in_train, index_col=cfg.data.index_col, parse_dates=cfg.data.date_cols
    )
    X_train_features = features_pipe.fit_transform(df_train)
    save_npz(out_train_feature, X_train_features)

    jb.dump(features_pipe, out_transformer)


if __name__ == "__main__":
    build_features()  # pylint: disable=no-value-for-parameter
