from typing import List

import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin

from src.configuration.config import DataConfig


class SitesBuilder(BaseEstimator, TransformerMixin):
    """
    Prepares CountVectorizer/TfidfVectorizer friendly list of strings with sites IDs
    Consumes pandas dataframe
    ----------
    Example
    [[1, 2, 3],
    [4, 5, 6]]
    --->
    ["1 2 3", "4 5 6"]
    """

    def __init__(self, data_cfg: DataConfig):
        self.site_columns = data_cfg.site_cols
        super().__init__()

    # pylint: disable=unused-argument
    def fit(self, X: pd.DataFrame, y=None) -> "SitesBuilder":
        return self

    def transform(self, X: pd.DataFrame, y=None) -> List[str]:
        # replace nan with 0 and convert to int
        X = X[self.site_columns].fillna(0).astype(int).values.tolist()
        # drop 0-values and join the rest
        return [" ".join(map(str, filter(lambda x: x != 0, row))) for row in X]
