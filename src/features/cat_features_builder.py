import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin

from src.configuration.config import DataConfig


class CatFeaturesBuilder(BaseEstimator, TransformerMixin):
    """
    Creates additional categorical features
    -----------------
    - dayofweek - day of week (0-6)
    - sites_count - number of sites in session (1-10)
    - week - # of week in year
    """

    def __init__(self, data_cfg: DataConfig):
        self.site_columns = data_cfg.site_cols
        self.date_columns = data_cfg.date_cols
        super().__init__()

    # pylint: disable=unused-argument
    def fit(self, X: pd.DataFrame, y=None) -> "CatFeaturesBuilder":
        return self

    def transform(self, X: pd.DataFrame, y=None) -> np.ndarray:
        dayofweek = X[self.date_columns[0]].dt.dayofweek
        sites_count = len(self.site_columns) - np.sum(
            X[self.site_columns].isna(), axis=1
        )
        week = X[self.date_columns[0]].dt.isocalendar().week
        return np.c_[dayofweek, sites_count, week]
