from typing import Dict

import mlflow
import numpy as np
import pandas as pd
from mlflow.models.model import ModelInfo
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import StratifiedKFold, cross_validate

from src.configuration import get_config
from src.models.mlflow_helper import get_sklearn_model_version


def evaluate(
    x_train: pd.DataFrame,
    y_train: np.ndarray,
    model_params: Dict[str, object],
) -> ModelInfo:
    """Cross-validate model, evaluates scores and sends results in mlflow

    Args:
        x_train (pd.DataFrame): train dataset
        y_train (np.ndarray): train target array
        model_params (Dict[str, str]): model parameters

    Returns:
        mlflow ModelInfo for newly created model
    """
    cfg = get_config()
    mlflow.set_experiment(cfg.mlflow.lr_experiment_name)
    mlf_client = mlflow.tracking.MlflowClient()
    with mlflow.start_run():
        # cross validate model
        cv = StratifiedKFold(n_splits=4, shuffle=True, random_state=17)
        lr = LogisticRegression(**model_params)
        metrics = ("roc_auc", "accuracy")
        cv_scores = cross_validate(
            lr, x_train, y_train, cv=cv, return_train_score=True, scoring=metrics
        )
        # leave only mean values of metrics
        scores = {
            metric: np.mean(val)
            for (metric, val) in cv_scores.items()
            if any(map(metric.endswith, metrics))
        }
        # build model
        lr = LogisticRegression(**model_params)
        lr.fit(x_train, y_train)
        # add model in mlflow and register it
        model_info = mlflow.sklearn.log_model(lr, cfg.mlflow.lr_model_name)
        model_version = mlflow.register_model(model_info.model_uri, cfg.mlflow.lr_model_name)
        mlflow.log_metrics(scores)
        mlflow.log_params(model_params)
        # link current transformer via model version tag
        transformer_version = get_sklearn_model_version(cfg.mlflow.transformer_model_name)
        mlf_client.set_model_version_tag(
            model_version.name,
            model_version.version,
            cfg.mlflow.transformer_tag,
            {
                "name": transformer_version.name,
                "version": transformer_version.version,
            },
        )

    return model_info
