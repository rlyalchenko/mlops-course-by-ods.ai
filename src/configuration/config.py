import logging
import os
from datetime import date, time
from typing import Dict, List, Optional, Tuple

from dotenv import load_dotenv
from mergedeep import merge
from pydantic import BaseModel
from yaml import safe_load


class DataConfig(BaseModel):
    """RAW data configuration"""

    index_col: str
    target_col: str
    site_cols: List[str]
    date_cols: List[str]


class EDAConfig(BaseModel):
    """EDA findings"""

    missing_week: int
    abnormal_day: date
    alice_timetable: Dict[str, List[Tuple[time, time]]]
    alice_themes: List[str]


class MLflowConfig(BaseModel):
    """Configuration settings for MLflow"""

    tracking_uri: str
    lr_model_name: str
    lr_experiment_name: str
    transformer_model_name: str
    transformer_experiment_name: str
    transformer_tag: str


class ModelServiceConfig(BaseModel):
    """Configuration setting for model web-service"""
    url: str
    predict_proba_method: str
    cache_timeout: int
    cache_size: int
    request_max_items: int


class Config(BaseModel):
    """All configurations"""

    data: DataConfig
    eda: EDAConfig
    mlflow: MLflowConfig
    model_service: ModelServiceConfig


_config: Optional[Config] = None


def get_config(config_path="conf/config.yaml") -> "Config":
    """Creates configuration settings object.
    Using provided config_path as default configuration settings file.
    Checks environment variable MLOPS_ODS_ALICE_CONF_ADDON for path to file,
    that overrides values in the default file.

    Args:
        config_path (str): path to default configuration file

    Returns:
        Config: configuration settings object
    """
    global _config
    if not _config:
        # load default config
        logging.info("Loading configuration file %s", config_path)
        with open(config_path, "r", encoding="utf-8") as f:
            cfg_values = safe_load(f)
        logging.info("Loaded configuration file %s", config_path)
        load_dotenv()
        # load config addon
        config_addon_path = os.getenv("MLOPS_ODS_ALICE_CONF_ADDON")
        if config_addon_path:
            logging.info("Loading configuration file addon %s", config_addon_path)
            with open(config_addon_path, "r", encoding="utf-8") as f:
                cfg_addon = safe_load(f)
                cfg_values = merge(cfg_values, cfg_addon)
            logging.info("Loaded configuration file addon %s", config_addon_path)
        _config = Config.parse_obj(cfg_values)

    return _config
