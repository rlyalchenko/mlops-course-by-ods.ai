import pandas as pd
import datetime
import click
import numpy as np

from src.configuration import get_config


@click.command()
@click.argument("in_train", type=click.Path(exists=True))
@click.argument("out_missed_added", type=click.Path())
def add_missing_data(in_train: str, out_missed_added: str):
    """Copies data from week 48 to week 49

    EDA shows week 49 absence is abnormal

    Args:
        in_train (str): cleaned file with train data (csv)
        output_file_path (str): train data with simulater week 49 (csv)
    """
    cfg = get_config()

    df_train = pd.read_csv(in_train, index_col=cfg.data.index_col, parse_dates=cfg.data.date_cols)
    # copy missing week from previous week
    # re-index new rows starting from max index in exising dataframe
    df_week49 = df_train[
        df_train[cfg.data.date_cols[0]].dt.isocalendar().week == cfg.eda.missing_week - 1
    ].copy()
    df_week49[cfg.data.date_cols] = df_week49[cfg.data.date_cols].applymap(
        lambda x: x + datetime.timedelta(days=7)
    )
    df_week49.reset_index(inplace=True)
    df_week49[cfg.data.index_col] = np.arange(
        df_train.index.max() + 1, df_train.index.max() + 1 + df_week49.shape[0]
    )
    df_week49.set_index(cfg.data.index_col, inplace=True)
    df_train = pd.concat([df_train, df_week49], ignore_index=False)
    df_train.to_csv(out_missed_added)


if __name__ == "__main__":
    add_missing_data()  # pylint: disable=no-value-for-parameter
