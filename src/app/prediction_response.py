from pydantic import BaseModel


class PredictionResponse(BaseModel):
    """Prediction response for session"""

    session_id: int
    class_0_proba: float
    class_1_proba: float
