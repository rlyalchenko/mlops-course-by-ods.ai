# MLOps Course by ods.ai

![pytest-cov](https://gitlab.com/rlyalchenko/mlops-course-by-ods.ai/-/jobs/artifacts/main/raw/pytest-coverage.svg?job=unittests)
![pytest](https://gitlab.com/rlyalchenko/mlops-course-by-ods.ai/-/jobs/artifacts/main/raw/pytest-status.svg?job=unittests)

## Описание
Домашний проект для курса MLOps от ods.ai https://ods.ai/tracks/ml-in-production-spring-22

В основу взято соревнование Kaggle Catch Me If You Can ("Alice") 
https://www.kaggle.com/c/catch-me-if-you-can-intruder-detection-through-webpage-session-tracking2

Этот проект является обновлением одного из финальных заданий 
специализации Coursera "Машинное обучение и анализ данных" 
https://www.coursera.org/specializations/machine-learning-data-analysis

## Описание задачи

Идентификация пользователя по его поведению в сети Интернет – это сложная и 
интересная задача на стыке анализа данных и поведенческой психологии. 
Эта задача решается как IT-гигантами, так стартапами и научными коллективами. 

В качестве примера, компания Яндекс решает задачу идентификации взломщика почтового 
ящика по его поведению. 
Взломщик будет себя вести не так, как владелец ящика: он может не удалять сообщения сразу 
по прочтении, он будет по-другому ставить флажки сообщениям и даже по-своему двигать мышкой. 
Тогда такого злоумышленника можно идентифицировать и "выкинуть" из почтового ящика, 
предложив хозяину войти по SMS-коду.

В этом проекте решается похожая задача: идентификация человека по последовательности из 
нескольких веб-сайтов, посещенных подряд.

## Структура репозитория

    ├── README.md           <- Этот файл
    │
    ├── conf                <- Файлы конфигурации
    ├── data
    │   ├── interim         <- Промежуточные данные
    │   ├── processed       <- Полностью обработанные файлы
    │   ├── raw             <- Оригинальные неизмененные файлы данных
    │   ├── submission      <- Предсказания полученной модели
    │   └── unittest        <- Данные для авто-тестов
    │
    ├── Docker             
    │   ├── .images         <- Docker-файлы дополнительных образов
    │   └── nginx           <- Конфигурация веб-сервера
    │
    ├── models              <- Рабочая папка для моделей: параметры, промежуточные модели и т.д.
    │
    ├── notebooks           <- Jupyter notebooks. Правила именования: номер, "-" (тире), 
    │                         описание через тире, например, 1.0-initial-data-exploration
    │
    ├── reports             <- Отчёты (HTML, PDF, pptx, etc.)
    │
    ├── src                 <- Исходный код (python)
    │   ├── app             <- Скрипты веб-сервиса модели
    │   ├── configuration   <- Скрипты по работе с настройками
    │   ├── data            <- Скрипты обработки данных
    │   ├── features        <- Скрипты работы с дополнительными признаками
    │   ├── models          <- Скрипты работы с моделями
    │   ├── tests           <- Авто-тесты
    │   └── visualization   <- Scripts to create exploratory and results oriented visualizations
    │
    ├── .env                <- Файл конфигурации для сервисов Docker, 
    │                           содержит ключи только для локальных сервисов
    │
    ├── reports             <- Отчёты (HTML, PDF, pptx, etc.)
    │  
    ├── .gitlab-ci.yml      <- Настойки ci/cd для gitlab
    │
    ├── docker-compose.yaml <- Файл развёртывая сервисов Docker
    │
    ├── dvc.yaml            <- Настройки DVC pipeline
    │
    ├── pyproject.toml      <- Стандартный файл проекта, включает настройки дополнительных компонент
    │
    └── tox.ini             <- tox file, используется только для хранения настроек flake8

## Соглашения по управлению качеством кода

Автоформатирование: 
- black (https://black.readthedocs.io/en/stable/)
- isort (https://pycqa.github.io/isort/)

Проверка кода (ci/cd): 
- code style: flake8, cтандартные настройки (https://flake8.pycqa.org/en/latest/)
- type cheсking: mypy, стандартные настройки и плагины (http://www.mypy-lang.org/)
- static code analyser: Pylint, адаптированная конфигурация Google (https://pylint.pycqa.org/en/latest/)

## Требования к системе
- Python 3.9. Версии 3.10 и выше не проверялись.
- poetry 1.1.13 и выше.
- Docker 20.10 и выше.

Корректность работы проверена на следующей конфигурации:
- Microsoft Windows 10 Pro Build 19044 
- Python 3.9.10 (tags/v3.9.10:f2f3f53, Jan 17 2022, 15:14:21)
- Poetry version 1.1.13
- Docker version 20.10.14, build a224086

## Установка и использование
1. Скачайте проект на локальный диск

    ```git clone https://gitlab.com/rlyalchenko/mlops-course-by-ods.ai.git```

1. Установите poetry
  
    ```pip install poetry```

1. Создайте виртуальное окружение и установите зависимости
   
    ```poetry install```

1. Разверните сервисы Docker

    ```docker compose up -d --build```
    
    Сервис minio-default-buckets закончит работу после создания необходимых бакетов в s3-хранилище.
    Его можно удалить
    
    ```docker rm minio-mc```

1. Запустите DVC pipeline
   
   ```poetry run dvc repro```

1. Опционально. Активировать хранение артефактов DVC в minio S3

    ```Windows PS  > .\dvc.init.ps1```
    
    ```Unix sh  $ ./dvc.init.sh```

=> Выполнена подготовка данных, обучены несколько моделей, подготовлен файл с результами обработки тестовых данных.

Для доступа к сервисам необходимо использовать ключи из файла .env

1. mlflow server http://localhost:5000/
2. mnnio s3 storage http://localhost:9001/
3. model web-service http://localhost:6060/docs


## Отчёты
- reports/Project Alice.pptx
  
  Презентация для финальной защиты проекта
